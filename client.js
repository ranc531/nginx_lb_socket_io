if (!process.stdout.isTTY)
    process.exit(0)

let host = `http://${process.argv[2]}`

console.log(`Connecting to ${host}`);
let socket = require('socket.io-client')(host);

socket.on('connect', () => {
    console.log('Connected');
    socket.emit('echo', 'hello');
    socket.disconnect();
});

socket.on('echo', (data) => console.log(`> ${data}`));

