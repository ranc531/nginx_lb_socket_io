# Test nginx lb

## Issue

Socket.io starts with polling and immediately upgrades, if we dont keep the
same IP with the same backend we end up polling with more than one server.

Easiest solution is to turn off the polling on the client side.

Other solution is to hash by ip but then you get NATted environments mixed up
in the polling, which can be overcome by using cookies and balance by them, or
identify some data from the query string.

## Structure

`lb` is the nginx server - with mapped `conf.d/default.conf`.

`socket1` & `socket2` is an echo websocket running on port 3000.

`client` is a client app which connects and sends a single echo request.

## Usage

* `npm i`
* `docker-compose up -d`
* `docker-compose run --rm client -- node client.js lb`

