let io = require('socket.io')();

io.on('connection', (socket) => {
    console.log('Client Connected');
    socket.on('echo', (data) => {
        console.log(`> ${data}`);
        socket.emit('echo', data);
    })
});

io.listen(3000);
